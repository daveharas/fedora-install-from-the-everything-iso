**# Fedora install from the Everything ISO & Work Station or any spin with BTRFS, Timeshift and Timeshift snapshots.**

I have written this guide after following this guide on you tube https://youtu.be/UDvZG4XyjTQ in which Stephen's Tech Talks walks you 
through the process in a very descriptive and simple format.

I would like to be able to repeat the process without watching the video again, as I tend 
to install Fedora from the everything ISO, however, this process will also work on all of the Fedora spins.

The installation includes how to setup BTRFS for adding "btrfs snapshots" to the grub menu. https://github.com/Antynea/grub-btrfs

Also how to use Anaconda to correctly setup BTRFS sub volumes.



